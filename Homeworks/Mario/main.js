var Url = "text.json";

var xmlHttp = new XMLHttpRequest();
xmlHttp.onreadystatechange = ProcessRequest;
xmlHttp.open( "GET", Url, true );
xmlHttp.send( null );

function ProcessRequest()
{
    if ( xmlHttp.readyState == 4 && xmlHttp.status == 200 )
    {
        var response = JSON.parse(xmlHttp.responseText);


        //*
        //* Sorting Column alphabeticaly
        //**


        response.sort(function(obj1, obj2){
            if (obj1.name <  obj2.name ) {
                return -1;
            }
            else if(obj1.name > obj2.name ) {
                return 1;
            }else {

                    return 0;
            }
        });

        printTable(response);


        /*
        **
        *  sorting column by numbers
         */
        //response.sort(function(obj1, obj2){
        //    if (obj1.declination < 0 && obj2.declination > 0) {
        //        return 1;
        //    }
        //    else if(obj1.declination > 0 && obj2.declination < 0) {
        //        return -1;
        //    }else {
        //        if(Math.abs(parseFloat(obj1.declination)) < Math.abs(parseFloat(obj2.declination))){
        //            return -1;
        //        }else if(Math.abs(parseFloat(obj1.declination)) > Math.abs(parseFloat(obj2.declination))){
        //            return 1;
        //        } else {
        //            return 0;
        //        }
        //    }
        //});
        //
        //printTable(response);
    }

}

function printTable(response) {
    document.write("<table border='1'>");

    for (var i = 0; i < response.length; i++) {
        if (i == 0) {
            printTableHeader(response[i]);
        }
        printObjectData(response[i]);
    }

    document.write("</table>");
}

function printTableHeader(obj) {
    document.write("<tr>");
    for(var key in obj) {
        document.write("<th>" + key + "</th>");
    }
    document.write("</tr>");
}

function printObjectData(obj) {
    document.write("<tr>");
    for(var key in obj) {
        if(typeof obj[key] !== 'object' || obj[key] === null) {
            document.write("<td>" + obj[key] + "</td>");
        }
        else {
            document.write("<td><table border='1'>");
            printTableHeader(obj[key]);
            printObjectData(obj[key]);
            document.write("</table></td>");
        }
    }
    document.write("</tr>");
}