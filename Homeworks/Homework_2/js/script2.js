/***
 * Fetches data from json file
 * @param none
 * returns void
 */

function loadJSON()
{
   var data_file = "json/data.json";
   var http_request = new XMLHttpRequest();
   try{
      // Opera 8.0+, Firefox, Chrome, Safari
      http_request = new XMLHttpRequest();
   }catch (e){
      // Internet Explorer Browsers
      try{
         http_request = new ActiveXObject("Msxml2.XMLHTTP");
      }catch (e) {
         try{
            http_request = new ActiveXObject("Microsoft.XMLHTTP");
         }catch (e){
            // Something went wrong
            alert("Your browser broke!");
            return false;
         }
      }
   }
   http_request.onreadystatechange  = function(){
      if (http_request.readyState == 4)
      {
         var jsonObj = JSON.parse(http_request.responseText),
             table = document.createElement("table");

         table.classList.add("jsonTbl");
         document.getElementById("myTable").appendChild(table);

         jsonObj = sortbyKey(jsonObj, 'rightAscension');

         drawTable(jsonObj, table);

      }
   }
   http_request.open("GET", data_file, true);
   http_request.send();
}

/***
 * Draws table in the html
 * @param object, table html element 
 * returns void
 */
function drawTable(obj, table){   
    for (var i = 0; i < obj.length; i++) {
        if (i == 0) {
            drawHeader(obj[i], table);
        }
        drawLines(obj[i], table);
    }
}

/***
 * Draws table head
 * @param object, table html element 
 * returns void
 */
function drawHeader(obj, table){
    var row = table.insertRow();

    for(var key in obj) {
        var cell = row.insertCell();
          cell.innerHTML = key;
          cell.className = "objTbl-th";
    }
}

/***
 * Draws table lines
 * @param object, table html element 
 * returns void
 */
function drawLines(obj, table){
    var row = table.insertRow();    

    for(var key in obj) {
      var cell = row.insertCell();

      if(typeof obj[key] !== 'object' || obj[key] === null){
         cell.innerHTML = obj[key];
      } else{         
         var innerTable = document.createElement('table');
         innerTable.className = "jsonTbl";

         drawHeader(obj[key], innerTable);
         drawLines(obj[key], innerTable);

         cell.appendChild(innerTable);
      }

      cell.className = "objTbl-td";
    }
}

/***
 * Sorts object by column name
 * @param object, column name 
 * returns {object}
 */
function sortbyKey(obj, key){   
   return obj.sort(function(obj1, obj2){
      var x = obj1[key],
          y = obj2[key];

      if(x > 0){
         return ((x < y) ? -1 : ((x > y) ? 1 : 0));
      } else{
         return ((x > y) ? -1 : ((x < y) ? 1 : 0));
      } 
   });          
}