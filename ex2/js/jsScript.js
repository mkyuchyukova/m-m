


// console.log(document.getElementsByClassName('header'));
// console.log(document.getElementById('footer'));

    (function (){
    var evenRows = document.querySelectorAll('table tr:nth-child(even)'),
        oddRows = document.querySelectorAll('table tr:nth-child(odd)'),
        keysElements = document.querySelectorAll('table tr:nth-child(even) > td:nth-child(odd)'),
        keyValues = document.querySelectorAll('table tr:nth-child(even) > td:nth-child(even)'),
        keysElementsOdd = document.querySelectorAll('table tr:nth-child(odd) > td:nth-child(odd)'),
        keyValuesOdd = document.querySelectorAll('table tr:nth-child(odd) > td:nth-child(even)'),
        objects = {
            id: 1,
            name: 'Pesho'
        },
        objectsOdd = {
            age: 25,
            gender: 'male'
        };


        function addClassToList(list, className){
            for(var i=list.length -1; i >= 0; i--){ // -- raboti po-byrzo ot ++
                list[i].classList.add(className);
            }
        }

        function addObjectToList(list, obj){
                for(var i=list.length-1; i >= 0; i--){
                    if (i%2){
                        list[i].innerHTML = obj[1];
                    }else{
                        list[i].innerHTML = obj[0];
                    }
            }
        }

        addClassToList(evenRows, 'evenClass');
        addClassToList(oddRows, 'oddClass');

        addObjectToList(keysElements, [Object.keys(objects)[0], Object.keys(objects)[1]]);
        addObjectToList(keyValues, [objects.id, objects.name]);

        addObjectToList(keysElementsOdd, [Object.keys(objectsOdd)[0], Object.keys(objectsOdd)[1]]);
        addObjectToList(keyValuesOdd, [objectsOdd.age, objectsOdd.gender]);
})();